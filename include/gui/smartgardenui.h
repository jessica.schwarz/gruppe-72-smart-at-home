#ifndef SMARTGARDENUI_H
#define SMARTGARDENUI_H

#include <QMainWindow>

namespace Ui {
class SmartgardenUI;
}

class SmartgardenUI : public QMainWindow
{
    Q_OBJECT

public:
    explicit SmartgardenUI(QWidget *parent = nullptr);
    ~SmartgardenUI();

private:
    Ui::SmartgardenUI *ui;
};

#endif // SMARTGARDENUI_H
