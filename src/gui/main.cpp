#include <QApplication>

#include "smartgardenui.h"

int main(int argc, char* argv[])
{
  QApplication app(argc, argv);

  SmartgardenUI mainWindow;
  mainWindow.show();

  return app.exec();
}
