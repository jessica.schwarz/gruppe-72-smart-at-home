#include "smartgardenui.h"
#include "ui_smartgardenui.h"

SmartgardenUI::SmartgardenUI(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::SmartgardenUI)
{
    ui->setupUi(this);
}

SmartgardenUI::~SmartgardenUI()
{
    delete ui;
}
