###############################################################################
################################# README ######################################
###############################################################################

This Readme provides help to configure this project to own needs.

# CONFIGURATION
We will use CLion as an example IDE for configuring this project so everything
builds accordingly.

## Requirements
- You have the git-repository already cloned for example in:
  ~/projects/git/smartgarden/
- You have a working installation of CLion and QtCreator

## Configuring for Linux

- Launch CLion, if this is your first time you will be prompted to:
  * Create a new project
  * New CMake Project from Sources
  * [[ Open ]]
  * Get from Version Control

- We will select [ Open ] and navigate to the folder where the git-repo was
  cloned to. (e.g. ~/projects/git/smartgarden)
- There, we will select the CMakeLists.txt file at the top of the source-tree
  (e.g. ~/projects/git/smartgarden/CMakeLists.txt)
- Doing this will let CLion parse the CMakeLists.txt and set everything up
  accordingly, given it finds all necessary REQUIRED packages, which are used
  in 'SmartGarden'
- In the top toolbar select [Build] -> [Build Project] to build the whole
  project in one go. If something fails, you are either missing DEPENDENCIES
  or have them misconfigured. Goto the Resolve-Problems section of this readme.

### Programming for GUI

- To be able to utilize the strengths, that come with Qt, one has to use
  QtCreator.
- Open QtCreator and select [Open Project] from the "Welcome Screen"
- As for CLion, navigate to the CMakeLists.txt in the projectdirectory on your
  machine (e.g. ~/projects/git/smartgarden/CMakeLists.txt)
- In a next step QtCreator greets you with "Configure Project":
  - We will keep it as "Desktop" and you can customize the build-directories to
    your own needs. Just make sure to NOT PUSH THEM TO THE GIT-REPO

## Configuring for Windows

- Configuring this project to be able to work on it on windows generally
  follows the same philosophie as for linux. The difference is, that Windows
  often has problems finding and linking the correct .dlls for Qt5.
- TODO: << EXPLAIN THIS PLOX >>

# Resolve-Problems

## CMAKE doesnt build my project (duh)


- If you use CLion you have the option to select [CMake] in the bottom left of
  the window. Once you are there, press the [Reload CMake Project] button and
  watch what kind of errors you get.
  Possible problems are:

  - CMake doesnt find a library:
    1) You have it not installed, do it now or do it never
    2) You have it installed and you are 100% sure that you did it. In that
       case check if you have your CMake installation in your path.
